# Indholdsfortegnelse

1. [Til læseren](#til-læseren)
2. [Brug af pressekittet](#brug-af-pressekittet)
3. [Bål & Brand pressetekst](#bål-brand-pressetekst)
4. [Musik](#musik)
5. [Kontakt](#kontakt)


# Til læseren

Hvis du ikke selv skal bruge dette pressekit, f.eks. blot skal finde et logo
eller lidt tekst, så **STOP OP**. Send _hele indholdet af dette pressekit_
til din grafiker, designer, tekstforfatter eller videoklipper. De ved hvad de
skal gøre med filerne. Det er så ægerligt at se et grumset billede. Du kan
også give dette direkte link:

https://gitlab.com/baalogbrand/pressekit/-/archive/master/pressekit-master.zip


# Brug af pressekittet

Dette pressekit indeholder en beskrivelse af Bål & Brand, som kan bruges i
promoveringsmateriale og lignende. Beskrivelsen ligger i rod-biblioteket og
findes i filformaterne `.txt`, `.pdf` samt `.docx`, så man kan bruge den der
er mest praktisk til formålet.

I mappen [`bandbilleder`](/bandbilleder/) ligger billeder til promovering og
øvrig pressebrug.

Mappen [`logos`](/logos/) indeholder forskellige logoer i filformaterne
`.eps`, `.svg` samt `.png`. Eksempler på logoerne kan ses i filen
[`logos/examples.pdf`](/logos/examples.pdf).


# Bål & Brand pressetekst

![Bål & Brand Logo][logo]

## Bål & Brands debutalbum Karensperiode får 4 stjerner i Gaffa og beskrives som _en charmerende, levende og musikalsk debut_

GFR beskrev i efteråret Bål & Brand som et band der er leveringsdygtige i
veleksekverede dansksprogede rocksange - og som et band med personlighed og
indignation.

Bål & Brand var i efteråret 2018 på tour som support for Christian Hjelm,
hvor bandet var rundt i hele landet fra Studenterhuset i Aalborg, Voxhall i
Århus til Vega i København og alt derimellem. Udover efterårstouren bød 2018
blandt andet også på festival på Møn, fyldt show på Bartof Station,
supportshow for den svenske trio Pale Honey på Loppen og dobbeltkoncerter med
Carsten Bjørnlunds Rovdrift og den amerikanske duo Ohmme og meget mere.

Bål & Brand er en Københavner-kvartet med masser af live erfaring. Bandet
spiller eksplosiv dansksproget rock der på en og samme tid forenes med
melodiske og følsomme numre. Musikken er fyldt med både trommer, der rusker i
én, guitar der til tider er fin og følsom og til tider syrer helt ud. Dette
blandet med en intens og følsom vokal skaber ørehængere i Bål & Brands helt
eget musikalske rock-univers.


# Musik

Lyt til Bål & Brand her: https://BaalogBrand.lnk.to/Karensperiode  
Facebook: https://www.facebook.com/baalogbrandband/  


# Kontakt

Bål & Brand  
Tlf.: 24 44 15 37  
Mail: info@baalogbrand.dk  


[logo]: /.static/baalogbrand_logo_black_small.png "Bål & Brand"
